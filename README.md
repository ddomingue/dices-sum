## Get distribution representation from sum of dice

Returns a norm estimate of the distribution of the sum of n dice

### Dependencies

* pip install scipy
* pip install numpy
* pip install matplotlib

# Run

python dice_estimation.py dice

+ dices : number of dice to run
