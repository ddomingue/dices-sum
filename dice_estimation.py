import sys
from scipy.stats import norm
import numpy as np
import matplotlib.pyplot as plt

# Estimate min sum value, max sum value, average and standard deviation
# for a sum of n dices


def estimate(n=2):
    min_sum = n
    max_sum = n * 6
    mean = n * 7 / 2
    variance = n * 35 / 12
    std = np.sqrt(variance)

    return min_sum, max_sum, mean, std


# Data display, generates a gaussian from estimations and plots
def plottyplot(n=2):
    min_sum, max_sum, mean, std = estimate(n)
    data = np.arange(min_sum, max_sum + 1, 1)
    print("N : {}, min_sum {}, max_sum {}, mean {}, std {}".format(
        n, min_sum, max_sum, mean, std))
    print(data)
    print("---------------------------------------")

    # generate norm distrib with E and standard deviation
    if n > 1:
        rv = norm(loc=mean, scale=std)
        plt.plot(data, rv.pdf(data), label="{} Dice".format(n))
        plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
                   ncol=2, mode="expand", borderaxespad=0.)
        # Comment if using range of norms
        plt.show()


def main(dice):
    plt.ylim(0., 0.18)
    plottyplot(dice)
    # Wanna draw a range of norms? use following instead of pottyplot(dices)
    #map(plottyplot, np.arange(1, dice + 1, 1))
    #plt.show()


if __name__ == "__main__":
    try:
        n = int(sys.argv[1])
    except:
        n = 2

    if n > 0:
        main(n)
    else:
        print("Not a valid number")
